# Risk Mapping Tool

🚀 We're live at: https://gitlab-org.gitlab.io/ci-cd/package-stage/risk-mapping/ 🚀

The Risk Mapping Tool was built with [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), [Jekyll](https://jekyllrb.com/) and [Boostrap](https://getbootstrap.com/). Uses [Issues API](https://docs.gitlab.com/ee/api/issues.html) and [Links API](https://docs.gitlab.com/ee/api/issue_links.html) to fetch data.

## Configuration

### Run locally

1. Clone this project
2. Run `bundle exec ruby fetcher.rb -p <risk_project_id>` to fetch risk issues from a specified project (Package Risk Mapping ID: `25891188` )
3. Run `jekyll serve --livereload` to serve the Page
4. Open `http://127.0.0.1:4000/ci-cd/package-stage/risk-mapping/`

**Note:** This is assuming Jekyll is installed on the machine. See [Jekyll installation instructions](https://jekyllrb.com/docs/installation/).

### Data

#### Labels

The tool depends on certain labels to process its data correctly. 

Labels used in this project:

* ~"risk" for risks that get to be pulled into the map. The issue tracker may contain other kinds of issues that are not risks per se.
* ~"risk::group-map" for general risks (Team, Quality, Infrastructure...) and do not fall under a Category
* ~"devops::package" and ~"group::package" to identify the group the map belongs to
* ~"Category::Package Registry", ~"Category::Container Registry" and ~"Category::Dependency Proxy" are applied on risk issues to categorise them 

#### Risk Score

The Risk Score is calculated: Impact Level X Probability. It is represented in the issue as it's Weight value.  

### Project Structure

```
.
+-- _config.yml ---> adjust Base URL to serve the website with a GitLab Group
+-- _data
|   +-- json/ ---> json database for issues and links
|   +-- vars.yml ---> configuration values to adjust when personalising
+-- _includes
+-- _layouts
+-- _categories ---> store category pages below and specify file_path in vars.yml 
|   +-- package_registry.html
|   +-- ...
+-- assets ---> images, icons
+-- _site
+-- index.html ---> group risk map, main page
+-- fetcher.rb ---> simple ruby script to fetch data to _data
```

